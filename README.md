

<h1> Usage: </h1>

&lt;**ftChart** **maxvalue**="100"
            **value**="100"
            **thickness**="5"
            **size**="100"
            **font**="Neris"
            **color**="#FFFF33"
            **bgcolor**="#AFAFAF"
            **fontcolor**="#000000"&gt;&lt;**/ftChart**&gt;


<h1>Attributes:</h1>

*  maxvalue - Maximum value of your chart
*  value - Current value of your chart
*  thickness - Border size
*  font - Number font in center of your chart
*  color - Color of fill
*  bgcolor - Color of background
*  fontcolor - Color of center font