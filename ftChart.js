window.addEventListener("load", function () {
  var ftCharts = document.getElementsByTagName("ftChart");
  for(var i = 0; i < ftCharts.length; i++){
    var cValues = ftCharts[i].attributes;
    var cState = Math.floor((cValues.value.value / cValues.maxvalue.value) * 4);
    var cDegRemain = ((cValues.value.value / cValues.maxvalue.value) * 360) % 90;
    var cPercent = Math.round((cValues.value.value / cValues.maxvalue.value) * 100);
    var cStyle = {
      main: `position: relative;width: ${cValues.size.value}px;height: ${cValues.size.value}px;`,
      state: [
        `transform: rotate(45deg);border: ${cValues.thickness.value}px solid ${cValues.bgcolor.value};border-radius: 50%;position: absolute; left:0px;top:0px;width:calc(100% - ${cValues.thickness.value * 2}px);height:calc(100% - ${cValues.thickness.value * 2}px);`,
        `transform: rotate(45deg);border: ${cValues.thickness.value}px solid ${cValues.bgcolor.value};border-top: ${cValues.thickness.value}px solid ${cValues.color.value};border-radius: 50%;position: absolute; left:0px;top:0px;width:calc(100% - ${cValues.thickness.value * 2}px);height:calc(100% - ${cValues.thickness.value * 2}px);`,
        `transform: rotate(45deg);border: ${cValues.thickness.value}px solid ${cValues.bgcolor.value};border-top: ${cValues.thickness.value}px solid ${cValues.color.value};border-right: ${cValues.thickness.value}px solid ${cValues.color.value};border-radius: 50%;position: absolute; left:0px;top:0px;width:calc(100% - ${cValues.thickness.value * 2}px);height:calc(100% - ${cValues.thickness.value * 2}px);`,
        `transform: rotate(45deg);border: ${cValues.thickness.value}px solid ${cValues.color.value};border-left: ${cValues.thickness.value}px solid ${cValues.bgcolor.value};border-radius: 50%;position: absolute; left:0px;top:0px;width:calc(100% - ${cValues.thickness.value * 2}px);height:calc(100% - ${cValues.thickness.value * 2}px);`,
        `transform: rotate(45deg);border: ${cValues.thickness.value}px solid ${cValues.color.value};border-radius: 50%;position: absolute; left:0px;top:0px;width:calc(100% - ${cValues.thickness.value * 2}px);height:calc(100% - ${cValues.thickness.value * 2}px);`,
      ],
      bPart: `width: 100%;height: 100%;position:absolute; top:0px; left: 0px;transform: rotate(${cState * 90}deg); display: ${cState == 4 ? "none" : "block"}`,
      halfPart: `width: 50%; height: 100%; position: absolute; left: 50%;overflow:hidden;`,
      borderPart: `width: ${cValues.size.value-(cValues.thickness.value * 2)}px;height: ${cValues.size.value-(cValues.thickness.value * 2)}px; margin-left: -${cValues.size.value/2}px;border: ${cValues.thickness.value}px solid #00000000; border-top: ${cValues.thickness.value}px solid ${cValues.color.value};border-radius: 50%;transform: rotate(${-45 + cDegRemain}deg);`,
      centerText: `width: 100%;height: 100%;line-height: ${cValues.size.value}px; vertical-align: middle; text-align: center; font-size: ${cValues.size.value/3}px;font-family: "${cValues.font.value}"; color: ${cValues.fontcolor.value};`
    }
    var innerHtml = `
      <div style="${cStyle.main}">
        <div style="${cStyle.state[cState]}"></div>
        <div style="${cStyle.bPart}">
          <div style="${cStyle.halfPart}">
            <div style="${cStyle.borderPart}"></div>
          </div>
        </div>
        <div style="${cStyle.centerText}">${cPercent}%</div>
      </div>
    `;
    ftCharts[i].innerHTML = innerHtml;
  }
});
